package app.com.dojo.page.common

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.tapendium.R

class AdapterSimpleTextList : RecyclerView.Adapter<ViewHolderSimpleText>() {

    interface OnAdapterSimpleTextListener {
        fun onSelectItemText(item: String)
    }

    private var textArray = ArrayList<String>()
    private var selectedText = ""
    private var listener: OnAdapterSimpleTextListener? = null

    init {
    }

    fun setTexts(array: ArrayList<String>) {
        textArray = array
        notifyDataSetChanged()
    }

    fun setSelect(text: String) {
        selectedText = text
        notifyDataSetChanged()
    }

    fun setListener(listener: OnAdapterSimpleTextListener) {
        this.listener = listener
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderSimpleText {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.adapter_simple_text, parent, false)
        return ViewHolderSimpleText(view, listener)
    }

    override fun onBindViewHolder(holder: ViewHolderSimpleText, position: Int) {
        holder.configure(textArray[position], selectedText == textArray[position])
    }

    override fun getItemCount(): Int {
        return textArray.count()
    }
}
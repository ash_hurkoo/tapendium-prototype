package app.com.dojo.page.common

import android.support.v7.widget.RecyclerView
import android.view.View
import com.app.tapendium.R
import kotlinx.android.synthetic.main.adapter_simple_text.view.*

class ViewHolderSimpleText(val view: View, val listener: AdapterSimpleTextList.OnAdapterSimpleTextListener?) : RecyclerView.ViewHolder(view) {

    init {
    }

    fun configure(text: String, selected: Boolean) {
        view.apply {
            textViewMain.text = text

            frameLayoutCellContainer.setBackgroundResource(
                when (selected) {
                    true -> {
                        R.drawable.bg_item_selected
                    }
                    false -> {
                        R.drawable.selector_item_cell
                    }
                }
            )

            listener?.let { listener ->
                rippleViewTouch.setOnClickListener {
                    listener.onSelectItemText(text)
                }
            }

        }
    }
}
package com.app.tapendium.common

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.com.dojo.page.common.AdapterImportantNotificationsList
import app.com.dojo.page.common.AdapterSingaporeList
import com.app.tapendium.R
import kotlinx.android.synthetic.main.fragment_horizontal_list.*

class HorizontalInformationListFragment: Fragment() {

    companion object {
        fun newInstance(headerTitle: String, type: Int = 0): HorizontalInformationListFragment {
            val fragment = HorizontalInformationListFragment()
            fragment.headerTitle = headerTitle
            fragment.type = type
            return fragment
        }
    }

    private lateinit var headerTitle: String
    private var type = 0

    private lateinit var adapterImportantNotificationsList: AdapterImportantNotificationsList
    private lateinit var adapterSingaporeList: AdapterSingaporeList

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_horizontal_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupList()
    }

    private fun setupList() {
        textViewInformationHeader.text = headerTitle

        val linearLayoutManager =LinearLayoutManager(context)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerViewInformationList.layoutManager = linearLayoutManager

        when (type) {
            0 -> {
                adapterImportantNotificationsList = AdapterImportantNotificationsList()
                recyclerViewInformationList.adapter = adapterImportantNotificationsList
            }
            1 -> {
                adapterSingaporeList = AdapterSingaporeList()
                recyclerViewInformationList.adapter = adapterSingaporeList
            }
        }
    }

}
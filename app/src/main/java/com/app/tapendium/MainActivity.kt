package com.app.tapendium

import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.widget.NestedScrollView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.View
import android.view.ViewTreeObserver
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import app.com.dojo.page.common.AdapterHighlightsSlider
import app.com.dojo.page.common.AdapterNotificationsList
import app.com.dojo.page.common.AdapterSimpleTextList
import com.app.tapendium.common.HorizontalInformationListFragment
import com.app.tapendium.hotel.ExploreHotelsFragment
import com.app.tapendium.uiutils.CustomTypefaceSpan
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_actions.*
import kotlinx.android.synthetic.main.layout_notifications.*
import kotlinx.android.synthetic.main.layout_select_lights.*
import kotlinx.android.synthetic.main.layout_slider.*
import kotlinx.android.synthetic.main.layout_topbar.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupImportantNotificationsList()
        setupExploreHotel()
        setupExploreSingapore()

        setupNotifications()
        setupLights()
        setupSlider()
    }

    private fun setupExploreHotel() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayoutExploreHotels, ExploreHotelsFragment.newInstance())
        transaction.commit()
    }

    private fun setupImportantNotificationsList() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayoutImportantNotificationsList, HorizontalInformationListFragment.newInstance("Important Notifications"))
        transaction.commit()
    }

    private fun setupExploreSingapore() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayoutExploreSingapore, HorizontalInformationListFragment.newInstance("Explore Singapore", 1))
        transaction.commit()
    }

    /*
    ** Notifications part
     */

    private var notificationsListShowing = false

    private fun setupNotifications() {
        frameLayoutNotifications.visibility = View.GONE

        rippleViewNotification.setOnClickListener {
            showAndHideNotificationsList()
        }

        frameLayoutNotifications.setOnClickListener {
            showAndHideNotificationsList()
        }

        val layoutManager = LinearLayoutManager(this)
        recyclerViewNotificationsList.layoutManager = layoutManager

        recyclerViewNotificationsList.adapter = AdapterNotificationsList()
    }

    private fun showAndHideNotificationsList() {
        notificationsListShowing = !notificationsListShowing
        if (notificationsListShowing) {
            frameLayoutNotifications.visibility = View.VISIBLE
            val animation = AnimationUtils.loadAnimation(this, R.anim.fade_in)
            animation.duration = 500
            frameLayoutNotifications.startAnimation(animation)
        } else {
            val animation = AnimationUtils.loadAnimation(this, R.anim.fade_out)
            animation.duration = 500
            animation.setAnimationListener(object: Animation.AnimationListener{
                override fun onAnimationRepeat(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                    frameLayoutNotifications.visibility = View.GONE
                }

                override fun onAnimationStart(p0: Animation?) {
                }
            })
            frameLayoutNotifications.startAnimation(animation)
        }
    }

    /*
    ** Lights part
     */

    private var lightsSelectBoxShowing = false

    private val screenLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
        adjustLightSelectBoxPosition()
        removeScreenLayoutListener()
    }

    private fun adjustLightSelectBoxPosition() {
        val locationOfContainer = IntArray(2)
        frameLayoutContainer.getLocationOnScreen(locationOfContainer)

        val locationOfLightBox = IntArray(2)
        frameLayoutLight.getLocationOnScreen(locationOfLightBox)

        val params = linearLayoutLightsBoxContainer.layoutParams as FrameLayout.LayoutParams
        params.leftMargin = locationOfLightBox[0] - locationOfContainer[0]
        params.topMargin = locationOfLightBox[1] - locationOfContainer[1]
        params.width = frameLayoutLight.width
        linearLayoutLightsBoxContainer.layoutParams = params
    }

    private fun removeScreenLayoutListener() {
        frameLayoutContainer.viewTreeObserver.removeOnGlobalLayoutListener(screenLayoutListener)
    }

    private fun setupLights() {
        frameLayoutLightsSelectContainer.visibility = View.GONE

        frameLayoutContainer.viewTreeObserver.addOnGlobalLayoutListener(screenLayoutListener)

        setupLightsList()

        rippleViewLight.setOnClickListener {
            showHideLightsSelectBox()
        }

        frameLayoutLightsSelectContainer.setOnClickListener {
            showHideLightsSelectBox()
        }

        scrollViewMain.setOnScrollChangeListener { _: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            adjustLightSelectBoxPosition()
        }
    }

    private fun setupLightsList() {
        recyclerViewLightActions.layoutManager = LinearLayoutManager(this)

        val adapter = AdapterSimpleTextList()
        adapter.setTexts(arrayListOf("I want all lights on", "I want to work", "I want to relax", "I want to sleep", "I want to bath"))
        recyclerViewLightActions.adapter = adapter

        adapter.setSelect("I want all lights on")
        adapter.setListener(object: AdapterSimpleTextList.OnAdapterSimpleTextListener {
            override fun onSelectItemText(item: String) {
                adapter.setSelect(item)

                textViewLight.text = item
                textViewLightOnSelectPanel.text = item
                showHideLightsSelectBox()
            }
        })
    }

    private fun showHideLightsSelectBox() {
        lightsSelectBoxShowing = !lightsSelectBoxShowing

        if (lightsSelectBoxShowing) {
            frameLayoutLightsSelectContainer.visibility = View.VISIBLE
        } else {
            frameLayoutLightsSelectContainer.visibility = View.GONE
        }
    }

    /*
    ** Slider part
     */

    private val adapterHighlightsSlider = AdapterHighlightsSlider()

    private val highlightsListViewLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
        adapterHighlightsSlider.setCellHeight(recyclerViewHighlights.height / 3)
        removeHighlightsListViewLayoutListener()
    }

    private fun removeHighlightsListViewLayoutListener() {
        recyclerViewHighlights.viewTreeObserver.removeOnGlobalLayoutListener(highlightsListViewLayoutListener)
    }

    private fun setupSlider() {

        val animationIn = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        animationIn.duration = 500
        viewFlipperMain.inAnimation = animationIn

        val animationOut = AnimationUtils.loadAnimation(this, R.anim.fade_out)
        animationOut.duration = 500
        viewFlipperMain.outAnimation = animationOut

        recyclerViewHighlights.layoutManager = LinearLayoutManager(this)
        recyclerViewHighlights.adapter = adapterHighlightsSlider
        recyclerViewHighlights.viewTreeObserver.addOnGlobalLayoutListener(highlightsListViewLayoutListener)

        textViewServiceTitle.textSize = resources.getDimension(R.dimen.slider_price_title_text_size)

        val font1 = Typeface.createFromAsset(assets, resources.getString(R.string.app_fgs_book))
        val font2 = Typeface.createFromAsset(assets, resources.getString(R.string.app_fgs_hvy))

        val spannableStringBuilder = SpannableStringBuilder("The Big\nBreakfast")
        spannableStringBuilder.setSpan(CustomTypefaceSpan("", font1), 0, "The".length, Spanned.SPAN_EXCLUSIVE_INCLUSIVE)
        spannableStringBuilder.setSpan(CustomTypefaceSpan("", font2), "The".length, "The Big\nBreakfast".length, Spanned.SPAN_EXCLUSIVE_INCLUSIVE)
        textViewServiceTitle.text = spannableStringBuilder

        adapterHighlightsSlider.setListener(object: AdapterHighlightsSlider.OnAdapterHighlightsSliderListener {
            override fun onSelectCell(position: Int) {
                viewFlipperMain.displayedChild = position
                adapterHighlightsSlider.setSelectedPosition(position)

                textViewCheckOut.text = if (position == 0) { "ORDER NOW" } else { "BOOK" }
                textViewServicePrice.visibility = if (position == 0) {View.VISIBLE} else {View.INVISIBLE}
                textViewServiceTitle.visibility = if (position == 0) {View.VISIBLE} else {View.INVISIBLE}
            }
        })
    }
}

package app.com.dojo.page.common

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.FrameLayout
import com.app.tapendium.R
import kotlinx.android.synthetic.main.adapter_highlights_slider.view.*

class ViewHolderHighlightsSlider(val view: View, private val listener: AdapterHighlightsSlider.OnAdapterHighlightsSliderListener?) : RecyclerView.ViewHolder(view) {

    init {
    }

    fun configure(height: Int, header: String, body: String, color: Int, textColor: Int, isSelected: Boolean, position: Int) {
        view.apply {
            val params = frameLayoutHighlightCellContainer.layoutParams as FrameLayout.LayoutParams
            params.height = height
            params.rightMargin =
                if (isSelected) {
                    0
                } else {
                    context.resources.getDimensionPixelSize(R.dimen.space_large)
                }
            frameLayoutHighlightCellContainer.layoutParams = params

            frameLayoutHighlightCellContainer.setBackgroundColor(color)
            textViewHeader.setTextColor(textColor)
            textViewHeader.text = header
            textViewBody.setTextColor(textColor)
            textViewBody.text = body

            listener?.let { listener ->
                rippleViewTouch.setOnClickListener {
                    listener.onSelectCell(position)
                }
            }
        }
    }
}
package app.com.dojo.page.common

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.tapendium.R

class AdapterHighlightsSlider : RecyclerView.Adapter<ViewHolderHighlightsSlider>() {

    interface OnAdapterHighlightsSliderListener {
        fun onSelectCell(position: Int)
    }

    private var cellHeight: Int = 100

    private var selectedPosition = 0

    private var listener: OnAdapterHighlightsSliderListener? = null

    private var displayData = arrayListOf(
        mapOf("header" to "Food Special", "body" to "The Big Breakfast", "color" to Color.WHITE, "textColor" to Color.parseColor("#203f3c")),
        mapOf("header" to "Featured Event", "body" to "Rock 'N' Roll 2019", "color" to Color.parseColor("#7a5599"), "textColor" to Color.WHITE),
        mapOf("header" to "SPA Offer", "body" to "30% off on Spa", "color" to Color.parseColor("#21b1c6"), "textColor" to Color.WHITE)
    )

    init {
    }

    fun setCellHeight(height: Int) {
        cellHeight = height
        notifyDataSetChanged()
    }

    fun setSelectedPosition(position: Int) {
        selectedPosition = position
        notifyDataSetChanged()
    }

    fun setListener(listener: OnAdapterHighlightsSliderListener) {
        this.listener = listener
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderHighlightsSlider {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.adapter_highlights_slider, parent, false)
        return ViewHolderHighlightsSlider(view, listener)
    }

    override fun onBindViewHolder(holder: ViewHolderHighlightsSlider, position: Int) {
        holder.configure(cellHeight,
            displayData[position]["header"] as String,
            displayData[position]["body"] as String,
            displayData[position]["color"] as Int,
            displayData[position]["textColor"] as Int,
            position == selectedPosition,
            position)
    }

    override fun getItemCount(): Int {
        return displayData.size
    }
}
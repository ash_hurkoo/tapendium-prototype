package com.app.tapendium.uiutils

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.Log
import android.widget.TextView
import com.app.tapendium.R

class TextViewCustomFont : TextView {

    private var fontFileName: String? = null

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        setFont(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setFont(context, attrs)
    }

    private fun setFont(context: Context, attrs: AttributeSet) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.CustomWidget)
        val s = a.getString(R.styleable.CustomWidget_font_file)

        if (s != null) {
            fontFileName = s.toString()
        }
        a.recycle()

        try {
            setFont(this, fontFileName ?: "")
        } catch (e: Throwable) {
            Log.e("Error", "Font setting error")
        }

        setLineSpacing(10f, 1f)
    }

    private fun setFont(textView: TextView, fontPath: String) {
        val tf = Typeface.createFromAsset(textView.context.assets, fontPath)
        textView.typeface = tf
    }
}
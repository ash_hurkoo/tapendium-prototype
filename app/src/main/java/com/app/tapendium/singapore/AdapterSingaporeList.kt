package app.com.dojo.page.common

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.tapendium.R

class AdapterSingaporeList : RecyclerView.Adapter<ViewHolderSingaporeList>() {

    private val singaporeCellData = arrayOf(
        mapOf("name" to "What To Do", "icon" to R.mipmap.img_sample_singapore_cell_01),
        mapOf("name" to "Tours", "icon" to R.mipmap.img_sample_singapore_cell_02),
        mapOf("name" to "Nightlife", "icon" to R.mipmap.img_sample_singapore_cell_03),
        mapOf("name" to "Eat", "icon" to R.mipmap.img_sample_singapore_cell_04),
        mapOf("name" to "Sports", "icon" to R.mipmap.img_sample_singapore_cell_05),
        mapOf("name" to "Shop", "icon" to R.mipmap.img_sample_singapore_cell_01),
        mapOf("name" to "Tours", "icon" to R.mipmap.img_sample_singapore_cell_02),
        mapOf("name" to "Nightlife", "icon" to R.mipmap.img_sample_singapore_cell_03),
        mapOf("name" to "Eat", "icon" to R.mipmap.img_sample_singapore_cell_04)
        )

    init {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderSingaporeList {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.adapter_singapore, parent, false)
        return ViewHolderSingaporeList(view)
    }

    override fun onBindViewHolder(holder: ViewHolderSingaporeList, position: Int) {
        holder.configure(singaporeCellData[position])
    }

    override fun getItemCount(): Int {
        return singaporeCellData.size
    }
}
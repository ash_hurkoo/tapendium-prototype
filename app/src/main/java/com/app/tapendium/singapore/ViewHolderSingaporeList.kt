package app.com.dojo.page.common

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.adapter_singapore.view.*

class ViewHolderSingaporeList(val view: View) : RecyclerView.ViewHolder(view) {

    init {
    }

    fun configure(cell: Map<String, Any>) {
        view.apply {
            textViewTitle.text = cell["name"] as String
            imageViewThumb.setImageResource(cell["icon"] as Int)
        }
    }
}
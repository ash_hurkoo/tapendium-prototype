package com.app.tapendium.hotel

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import app.com.dojo.page.common.AdapterExploreHotel
import com.app.tapendium.R
import kotlinx.android.synthetic.main.fragment_explore_hotels.*

class ExploreHotelsFragment: Fragment() {

    companion object {
        fun newInstance() = ExploreHotelsFragment()
    }

    private lateinit var adapter: AdapterExploreHotel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_explore_hotels, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupList()
    }

    private val globalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
        adapter.setTargetWidth((frameLayoutContainer.width - 4 * resources.getDimensionPixelSize(R.dimen.space_middle)) / 5)
        removeGlobalLayoutListener()
    }

    private fun removeGlobalLayoutListener() {
        frameLayoutContainer.viewTreeObserver.removeOnGlobalLayoutListener(globalLayoutListener)
    }

    private fun setupList() {
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerViewMain.layoutManager = linearLayoutManager

        adapter = AdapterExploreHotel()
        recyclerViewMain.adapter = adapter

        frameLayoutContainer.viewTreeObserver.addOnGlobalLayoutListener(globalLayoutListener)
    }

}
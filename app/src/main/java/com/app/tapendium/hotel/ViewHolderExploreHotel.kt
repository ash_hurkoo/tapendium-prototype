package app.com.dojo.page.common

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.FrameLayout
import com.app.tapendium.R
import kotlinx.android.synthetic.main.adapter_explore_hotel.view.*

class ViewHolderExploreHotel(val view: View) : RecyclerView.ViewHolder(view) {

    init {
    }

    fun configure(displayData: Map<String,  Any>, targetWidth: Int, isLastItem: Boolean) {
        view.apply {
            val params = linearLayoutContainer.layoutParams as FrameLayout.LayoutParams
            params.width = targetWidth
            params.rightMargin = if (isLastItem) { 0 } else { context.resources.getDimensionPixelSize(R.dimen.space_middle) }
            linearLayoutContainer.layoutParams = params

            textViewTitle.text = displayData["name"] as String
            imageViewIcon.setImageResource(displayData["icon"] as Int)
        }
    }
}
package app.com.dojo.page.common

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.tapendium.R

class AdapterExploreHotel : RecyclerView.Adapter<ViewHolderExploreHotel>() {

    private var targetWidth: Int = 0

    private val exploreHotelCellData = arrayOf(
        mapOf("name" to "Control\nTemperature", "icon" to R.mipmap.icon_temperature_yellow),
        mapOf("name" to "Control\nRoom Lighting", "icon" to R.mipmap.icon_lighting_yellow),
        mapOf("name" to "Order\nFood", "icon" to R.mipmap.icon_food_yellow),
        mapOf("name" to "Spa\nPackages", "icon" to R.mipmap.icon_spa_packages_yellow),
        mapOf("name" to "Shop from\nFairmont", "icon" to R.mipmap.icon_shopping_yellow))

    init {
    }

    fun setTargetWidth(width: Int) {
        targetWidth = width
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderExploreHotel {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.adapter_explore_hotel, parent, false)
        return ViewHolderExploreHotel(view)
    }

    override fun onBindViewHolder(holder: ViewHolderExploreHotel, position: Int) {
        holder.configure(exploreHotelCellData[position], targetWidth, position == 4)
    }

    override fun getItemCount(): Int {
        return 5
    }
}
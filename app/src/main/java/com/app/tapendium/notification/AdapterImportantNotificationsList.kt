package app.com.dojo.page.common

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.tapendium.R

class AdapterImportantNotificationsList : RecyclerView.Adapter<ViewHolderImportantNotifications>() {

    init {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderImportantNotifications {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.adapter_important_notification, parent, false)
        return ViewHolderImportantNotifications(view)
    }

    override fun onBindViewHolder(holder: ViewHolderImportantNotifications, position: Int) {
        holder.configure()
    }

    override fun getItemCount(): Int {
        return 2
    }
}
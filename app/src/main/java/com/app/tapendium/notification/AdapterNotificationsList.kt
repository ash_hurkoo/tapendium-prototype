package app.com.dojo.page.common

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.tapendium.R

class AdapterNotificationsList : RecyclerView.Adapter<ViewHolderNotifications>() {

    init {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderNotifications {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.adapter_notification, parent, false)
        return ViewHolderNotifications(view)
    }

    override fun onBindViewHolder(holder: ViewHolderNotifications, position: Int) {
        holder.configure()
    }

    override fun getItemCount(): Int {
        return 15
    }
}
package app.com.dojo.page.common

import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.View
import com.app.tapendium.R
import com.app.tapendium.uiutils.CustomTypefaceSpan
import kotlinx.android.synthetic.main.adapter_important_notification.view.*

class ViewHolderImportantNotifications(val view: View) : RecyclerView.ViewHolder(view) {

    init {
    }

    fun configure() {
        view.apply {
            val font1 = Typeface.createFromAsset(context.assets, context.resources.getString(R.string.app_fgs_book))
            val font2 = Typeface.createFromAsset(context.assets, context.resources.getString(R.string.app_fgs_med))

            textViewMessage.textSize = context.resources.getDimension(R.dimen.action_buttons_text_size)
            val spannableStringBuilder = SpannableStringBuilder("Hope you have enjoyed your stay. Checkout time is 10:00 AM")
            spannableStringBuilder.setSpan(CustomTypefaceSpan("", font1), 0, "Hope you have enjoyed your stay. ".length, Spanned.SPAN_EXCLUSIVE_INCLUSIVE)
            spannableStringBuilder.setSpan(CustomTypefaceSpan("", font2), "Hope you have enjoyed your stay. ".length, "Hope you have enjoyed your stay. Checkout time is 10:00 AM".length, Spanned.SPAN_EXCLUSIVE_INCLUSIVE)
            textViewMessage.text = spannableStringBuilder
        }
    }
}